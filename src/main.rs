use std::path::PathBuf;
use std::fs;

use structopt::StructOpt;
use byteorder::{ByteOrder, BigEndian};
use alpheratz_rust::Alpheratz;

#[derive(StructOpt)]
#[structopt(name = "alpheratz")]
struct CliOptions {
    /// Enable stack trace output
    #[structopt(short = "t", long, parse(from_flag))]
    stack_trace: bool,

    /// Source file to be executed
    #[structopt(name = "FILE", parse(from_os_str))]
    file: PathBuf,
}

fn main() -> Result<(), String> {
    env_logger::init();

    let args = CliOptions::from_args();
    let file_contents = fs::read(&args.file)
        .map_err(|err| format!("Error reading file \"{:?}\": {}", &args.file, err))?;
    let mut code: Vec<i64> = Vec::new();
    for i in (0..file_contents.len()).step_by(8) {
        code.push(BigEndian::read_i64(&file_contents[i..i+8]));
    }

    let mut vm = Alpheratz::new(code, 2048, 16);
    vm.run(args.stack_trace);

    Ok(())
}
