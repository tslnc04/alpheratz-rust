//! `alpheratz_rust` is the package used internally for the rust
//! implementation of the alpheratz virtual machine. Actually using the
//! virtual machine can be done through invoking the executable. Usage
//! is printed with the command line argument `--help`.

#![warn(missing_debug_implementations, rust_2018_idioms, missing_docs)]

mod opcode;
use opcode::Opcode;
use log::{debug, info};

/// `Alpheratz` is the struct used to represent the CPU that the virtual
/// machine runs on.
///
/// # Internals
/// 
/// Internally the CPU uses vectors of `i64` to represent instructions,
/// the stack, and globals. The instruction, frame, and stack pointers
/// are represented using `usize`. Flags of zero and sign are
/// represented as booleans. Finally, there is a boolean used to check
/// if the stack trace is enabled. Currently this only prints some
/// diagnostics every time a new instruction is run.

#[derive(Debug)]
pub struct Alpheratz {
    instructions: Vec<i64>,
    stack: Vec<i64>,
    globals: Vec<i64>,

    ip: usize,
    fp: usize,
    sp: usize,

    zero: bool,
    sign: bool,

    stack_trace: bool,
}

impl Alpheratz {
    /// `new` creates a new Alpheratz struct with the given instructions
    /// of `Vec<i64>` and stack size and global counts of type `usize`.
    ///
    /// Although more than the allotted stack space and number of
    /// globals can be used, space is allocated for the stack and global
    /// vectors when the struct is created in order to minimize
    /// allocations and reallocations. For this reason it is best to set
    /// these values to be a nice medium between overly big and so small
    /// there are many allocations.

    pub fn new(instructions: Vec<i64>, stack_size: usize, global_count: usize) -> Alpheratz {
        Alpheratz {
            instructions: instructions,
            stack: Vec::with_capacity(stack_size),
            globals: Vec::with_capacity(global_count),

            ip: 0,
            fp: 0,
            sp: 0,

            zero: false,
            sign: false,

            stack_trace: false,
        }
    }

    fn next_instruction(&mut self) -> Option<i64> {
        let instruction = self.instructions.get(self.ip)?;
        self.ip += 1;
        Some(*instruction)
    }

    fn next_opcode(&mut self) -> Opcode {
        let instruction = self.next_instruction();

        match instruction {
            Some(op) => Opcode::from(op),
            None => Opcode::HALT,
        }
    }

    /// `run` starts the process of executing the code vector within the
    /// virtual machine. It takes only a boolean corresponding to
    /// whether a stack trace is enabled. If enabled, general
    /// diagnostics will be printed when each instruction is executed,
    /// allowing for easier debugging of programs.

    pub fn run(&mut self, stack_trace: bool) {
        self.stack_trace = stack_trace;

        while (self.ip as usize) < self.instructions.len() {
            let op = self.next_opcode();

            match op {
                Opcode::PUSH => self.push(),
                Opcode::POP => {
                    self.pop();
                }

                Opcode::ADD => self.do_binary_op(|left, right| left + right),
                Opcode::SUB => self.do_binary_op(|left, right| left - right),
                Opcode::MUL => self.do_binary_op(|left, right| left * right),
                Opcode::DIV => self.do_binary_op(|left, right| left / right),

                Opcode::JMP => self.jmp(),
                Opcode::JSZ => self.stack_cond_jump(|stack_top| stack_top == 0),
                Opcode::JSNZ => self.stack_cond_jump(|stack_top| stack_top != 0),
                Opcode::JSP => self.stack_cond_jump(|stack_top| stack_top >= 0),
                Opcode::JSN => self.stack_cond_jump(|stack_top| stack_top < 0),

                Opcode::CMP => self.cmp(),
                Opcode::JE => self.flag_cond_jump(|zero, _sign| zero),
                Opcode::JNE => self.flag_cond_jump(|zero, _sign| !zero),
                Opcode::JG => self.flag_cond_jump(|zero, sign| !zero && !sign),
                Opcode::JGE => self.flag_cond_jump(|zero, sign| zero || !sign),
                Opcode::JL => self.flag_cond_jump(|zero, sign| !zero && sign),
                Opcode::JLE => self.flag_cond_jump(|zero, sign| zero || sign),

                Opcode::AND => self.do_binary_op(|left, right| left & right),
                Opcode::OR => self.do_binary_op(|left, right| left | right),
                Opcode::XOR => self.do_binary_op(|left, right| left ^ right),
                Opcode::NOT => self.do_unary_op(|arg| !arg),

                Opcode::EMIT => self.emit(),

                Opcode::LSHIFT => self.do_binary_op(|left, right| left << right),
                Opcode::RSHIFT => self.do_binary_op(|left, right| left >> right),

                Opcode::LOAD => self.load(),
                Opcode::GLOAD => self.gload(),
                Opcode::STORE => self.store(),
                Opcode::GSTORE => self.gstore(),

                Opcode::DUP => self.dup(),
                Opcode::ALLOC => self.alloc(),

                Opcode::CALL => self.call(),
                Opcode::RET => self.ret(),

                // using an if is faster than (left == right) as i64
                // https://stackoverflow.com/questions/55461617/how-do-i-convert-a-boolean-to-an-integer-in-rust#comment102403300_55461689
                Opcode::CMPEQ => self.do_binary_op(|left, right| if left == right { 1 } else { 0 }),
                Opcode::CMPNE => self.do_binary_op(|left, right| if left != right { 1 } else { 0 }),
                Opcode::CMPG => self.do_binary_op(|left, right| if left > right { 1 } else { 0 }),
                Opcode::CMPGE => self.do_binary_op(|left, right| if left >= right { 1 } else { 0 }),
                Opcode::CMPL => self.do_binary_op(|left, right| if left < right { 1 } else { 0 }),
                Opcode::CMPLE => self.do_binary_op(|left, right| if left <= right { 1 } else { 0 }),

                Opcode::MOD => self.do_binary_op(|left, right| left % right),
                Opcode::POW => self.do_binary_op(|left, right| left.pow(right as u32)),
                Opcode::ROOT => {
                    self.do_binary_op(|left, right| (left as f64).powf(right as f64).round() as i64)
                }

                Opcode::NOP => continue,
                Opcode::HALT | _ => {
                    info!("Halting...");
                    break;
                }
            }

            if self.stack_trace {
                // debug!("ip={} opcode={:?} sp={} fp={} stack={:?}", self.ip, op, self.sp, self.fp, &self.stack[..self.sp]);
                debug!("{:?}", &self.stack[..self.sp]);
            }
        }
    }

    fn push_val(&mut self, val: i64) {
        if (self.sp) >= self.stack.len() {
            self.stack.push(val);
            self.sp += 1;
        } else {
            self.stack[self.sp] = val;
            self.sp += 1;
        }
    }

    fn push(&mut self) {
        if let Some(next) = self.next_instruction() {
            self.push_val(next)
        }
    }

    fn pop(&mut self) -> Option<i64> {
        let stack_top = self.stack.get(self.sp - 1)?;
        self.sp -= 1;
        Some(*stack_top)
    }

    fn do_unary_op(&mut self, op: fn(i64) -> i64) {
        if self.sp < 1 {
            panic!("Not enough args on stack for unary op");
        }

        let arg = self.pop().unwrap();

        self.push_val(op(arg))
    }

    fn do_binary_op(&mut self, op: fn(i64, i64) -> i64) {
        if self.sp < 2 {
            panic!("Not enough args on stack for binary op");
        }

        // it feels more natural to push left then right
        // thus, right must be popped first
        let right = self.pop().unwrap();
        let left = self.pop().unwrap();

        self.push_val(op(left, right));
    }

    fn jmp(&mut self) {
        if self.sp < 1 {
            panic!("Not enough args on stack for jmp");
        }

        let jump_addr = self.pop().unwrap() as usize;

        if jump_addr <= self.instructions.len() {
            self.ip = jump_addr;
        }
    }

    fn stack_cond_jump(&mut self, cond: fn(i64) -> bool) {
        if self.sp < 2 {
            panic!("Not enough args on stack for stack cond jump");
        }

        let jump_addr = self.pop().unwrap() as usize;
        let stack_top = self.pop().unwrap();

        if cond(stack_top) && jump_addr < self.instructions.len() {
            self.ip = jump_addr;
        }
    }

    fn cmp(&mut self) {
        if self.sp < 2 {
            panic!("Not enough args on stack for cmp");
        }

        let right = *self.stack.get(self.sp - 1).unwrap();
        let left = *self.stack.get(self.sp - 2).unwrap();
        let result = left - right;

        if result == 0 {
            self.zero = true;
            self.sign = false;
        } else if result < 0 {
            self.zero = false;
            self.sign = true;
        } else {
            self.zero = false;
            self.sign = false;
        }
    }

    fn flag_cond_jump(&mut self, cond: fn(bool, bool) -> bool) {
        if self.sp < 1 {
            panic!("Not enough args on stack for flag cond jump");
        }

        let jump_addr = self.pop().unwrap() as usize;

        if cond(self.zero, self.sign) && jump_addr < self.instructions.len() {
            self.ip = jump_addr;
        }
    }

    fn emit(&mut self) {
        if self.sp < 1 {
            panic!("Not enough args on stack for emit");
        }

        let val = self.pop().unwrap();

        println!("=============== Program Output ===============\n{}\n==============================================", val);
    }

    fn load(&mut self) {
        if self.sp < 1 {
            panic!("Not enough args on stack for load");
        }

        let load_addr = self.pop().unwrap();
        let abs_addr = (self.fp as i64 + load_addr) as usize;

        if let Some(stack_at_addr) = self.stack.get(abs_addr) {
            let deref_stack_at_addr = *stack_at_addr;
            self.push_val(deref_stack_at_addr);
        }
    }

    fn gload(&mut self) {
        if self.sp < 1 {
            panic!("Not enough args on stack for gload");
        }

        let load_addr = self.pop().unwrap() as usize;
        if let Some(global) = self.globals.get(load_addr) {
            let deref_global = *global;
            self.push_val(deref_global);
        }
    }

    fn store(&mut self) {
        if self.sp < 2 {
            panic!("Not enough args on stack for store");
        }

        let store_addr = self.pop().unwrap();
        let abs_addr = (self.fp as i64 + store_addr) as usize;
        let local = self.pop().unwrap();

        if abs_addr < self.stack.len() {
            self.stack[abs_addr] = local;
        }
    }

    fn gstore(&mut self) {
        if self.sp < 2 {
            panic!("Not enough args on stack for gstore");
        }

        let store_addr = self.pop().unwrap() as usize;
        let global = self.pop().unwrap();

        if store_addr < self.globals.len() {
            self.globals[store_addr] = global;
        } else {
            self.globals.push(global);
        }
    }

    fn dup(&mut self) {
        if self.sp < 1 {
            panic!("Not enough args on stack for dup");
        }

        let stack_top = *self.stack.get(self.sp-1).unwrap();
        self.push_val(stack_top);
    }

    fn alloc(&mut self) {
        if self.sp < 2 {
            panic!("Not enough args on stack for alloc");
        }

        let alloc_space = self.pop().unwrap();
        let alloc_addr = self.pop().unwrap();
        let abs_addr = (self.fp as i64 + alloc_addr) as usize;

        // TODO: add checking for stack overflow
        for i in 0usize..(alloc_space as usize) {
            if abs_addr + i >= self.stack.len() {
                self.stack.push(0);
            } else {
                self.stack[abs_addr+i] = 0;
            }

            if abs_addr + i >= self.sp {
                self.sp = abs_addr + i + 1;
            }
        }
    }

    fn call(&mut self) {
        if self.sp < 2 {
            panic!("Not enough args on stack for call");
        }

        let argc = self.pop().unwrap();
        let fn_addr = self.pop().unwrap() as usize;

        self.push_val(argc);
        self.push_val(self.fp as i64);
        self.push_val(self.ip as i64);

        self.fp = self.sp;
        self.ip = fn_addr;
    }

    fn ret(&mut self) {
        if self.sp < 4 {
            panic!("Not enough args on stack for ret");
        }

        let retval = self.pop().unwrap();
        
        self.sp = self.fp;
        self.ip = self.pop().unwrap() as usize;
        self.fp = self.pop().unwrap() as usize;

        self.sp = (self.sp as i64 - self.pop().unwrap()) as usize;

        self.push_val(retval);
    }
}
