# alpheratz-rust

Rust implementation of the [Alpheratz VM](https://github.com/tslnc04/andromeda/blob/master/vm.go).

## What?

Alpheratz is the name of the virtual machine I wrote for my never to be completed programming language named Andromeda (Alpheratz is the brightest star, or rather binary star system, in the Andromeda galaxy).

In addition to the virtual machine implementation written in Rust, there is a quick and dirty implementation of an assembler and disassembler written in Python.

## Using

The assembler and disassembler can be run using the following command, with `a` setting the mode to assembly and `d` setting the mode to disassembly.

```shell
python3 asm.py <mode> <file>
```

The actual virtual machine has documentation attached for itself. However, logging is based on the environment variable `RUST_LOG`, so this must be set to properly see the output of the virtual machine.


```shell
cargo build; RUST_LOG=trace target/debug/alpheratz-rust [option] [file]
```

## Works in Progress
- [ ] Better error handling (way too many panics)
- [ ] Tests (I'm terrible at TDD)

## Contributing
Contributions are welcome either as pull requests or issues.

## Credits

Repository logo courtesy of [Stockholm University](https://astro.su.se/english).

## Licensing

Licensed under MIT.

Copyright 2020 Timothy Laskoski
