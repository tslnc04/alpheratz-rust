#!/usr/local/bin/python3

import sys

fname = "bytecode.vm"
mode = "a"

if len(sys.argv) > 2:
    mode = sys.argv[1]
    fname = sys.argv[2]

inst_to_op = {
    0: "PUSH",
    1: "POP",
    2: "ADD",
    3: "SUB",
    4: "MUL",
    5: "DIV",
    6: "JMP",
    7: "JSZ",
    8: "JSNZ",
    9: "JSP",
    10: "JSN",
    11: "CMP",
    12: "JE",
    13: "JNE",
    14: "JG",
    15: "JGE",
    16: "JL",
    17: "JLE",
    18: "AND",
    19: "OR",
    20: "XOR",
    21: "NOT",
    22: "EMIT",
    23: "NOP",
    24: "LSHIFT",
    25: "RSHIFT",
    26: "HALT",
    27: "LOAD",
    28: "CALL",
    29: "RET",
    30: "STORE",
    31: "GLOAD",
    32: "GSTORE",
    33: "DUP",
    34: "ALLOC",
    35: "CMPEQ",
    36: "CMPNE",
    37: "CMPG",
    38: "CMPGE",
    39: "CMPL",
    40: "CMPLE",
    41: "MOD",
    42: "POW",
    43: "ROOT",
}

op_to_inst = {v: k for k, v in inst_to_op.items()}

# assemble mode

if mode == "a":
    output = []
    labels = {}

    with open(fname, "r") as f:
        for line in f.readlines():
            ops = line.strip().split()

            if ops[0].startswith(";"):
                continue

            if ops[0].upper() == "LABEL":
                labels[ops[1]] = len(output)
                continue
            elif ops[0].startswith("$"):
                output += [x for x in op_to_inst["JMP"].to_bytes(8, byteorder="big", signed=True)]
                output += [x for x in labels[ops[0][1:]].to_bytes(8, byteorder="big", signed=True)]
                continue

            output += [x for x in op_to_inst[ops[0].upper()].to_bytes(8, byteorder="big", signed=True)]
            output += [x for op in ops[1:] for x in int(op).to_bytes(8, byteorder="big", signed=True)]

    with open(fname[:-3] + ".out", "wb") as f:
        f.write(bytearray(output))
else:
    with open(fname, "rb") as f:
        data = f.read()
        endian_data = [int.from_bytes(data[x:x+8], "big", signed=True) for x in range(0, len(data), 8)]

        i = 0
        while i < len(endian_data):
            if endian_data[i] in inst_to_op:
                name = inst_to_op[endian_data[i]]
            else:
                print(f"Unknown instruction with value {endian_data[i]}")
                i += 1
                continue

            if name == "PUSH":
                print(f"PUSH\t{endian_data[i+1]}")
                i += 2
            else:
                print(name)
                i += 1
